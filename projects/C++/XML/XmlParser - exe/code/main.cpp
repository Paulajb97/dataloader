//Author: Paula Jim�nez Benito
//Date: 2019


#include <iostream>
using namespace std;

#include "headers\Xml_Parser.hpp"
using namespace parser;


int main(int argc, char *argv[]) {

	Xml_Parser xml_parser;

	//Load xml from command args
	if (argc != 1) {
		xml_parser.load(argv[1]);
	}

	//Load xml from string
	//xml_parser.load("../../assets/configs.xml");

	//Print saved data
	for (int i = 0; i < xml_parser.get_loaded_entities_count(); ++i) {

		cout << "--- Entity " << i << " ---" << endl;
		cout << "Name: " << xml_parser.get_name(i) << endl;
		cout << "Position: " << xml_parser.get_pos_x(i) << ", " << xml_parser.get_pos_y(i) << ", " << xml_parser.get_pos_z(i) << endl;
		cout << endl;

	}
	

	//xml_parser.clear_saved();
	//xml_parser.add_entity_to_save(0, "Hiiiiiiiiii", 10, 20, 30);
	//xml_parser.save();


	cin.get();

	return 0;
}