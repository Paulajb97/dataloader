//Author: Paula Jim�nez Benito
//Date: 2019


#include "stdafx.h"

#include "Xml_Parser.h"

#include "rapidxml_utils.hpp"
#include "rapidxml_print.hpp"
#include <sstream>
#include <fstream>


namespace parser {


	void Xml_Parser::load_xml(std::string path)
	{

		//Read xml
		rapidxml::file<> xml_asset(path.c_str());
		std::stringstream buffer;
		buffer << xml_asset.data();
		std::string content(buffer.str());

		//Parse
		rapidxml::xml_document<> doc;
		doc.parse<0>(&content[0]);

		xml_node<>* root = doc.first_node();
		if (root) {

			//Entities
			for (xml_node<>* child = root->first_node(); child; child = child->next_sibling())
				parse_entity(child);

		}

	}

	void Xml_Parser::parse_entity(xml_node<>* entity_tag)
	{

		//Entity values
		int id;
		std::string name;
		float pos[3];


		//Data

		id = std::atoi(entity_tag->first_attribute("id")->value());								//Id

		for (rapidxml::xml_node<> * child = entity_tag->first_node(); child; child = child->next_sibling()) {

			if (child->type() == rapidxml::node_element) {

				if (std::string(child->name()) == "name") {										//Name

					name = child->value();

				}

				if (std::string(child->name()) == "position") {									//Position

					std::string result = child->value();

					int count = 0;
					std::string num = "";

					for (std::string::iterator it = result.begin(); it != result.end(); ++it) {	//Value iteration

						if (*it != ',' && *it != ' ') {
							num += *it;
						}
						else {

							pos[count] = (float)std::atof(num.c_str());
							++count;
							num = "";

						}

						if (it == --result.end()) {
							pos[count] = (float)std::atof(num.c_str());
						}

					}

				}
			}
		}

		//Save entity
		entities_loaded.push_back(new Entity(id, name, pos[0], pos[1], pos[2]));

	}

	void Xml_Parser::write_into_xml(std::string path)
	{

		xml_document<> doc;
		xml_node<>* decl = doc.allocate_node(node_declaration);
		decl->append_attribute(doc.allocate_attribute("version", "1.0"));
		decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
		doc.append_node(decl);

		//Root
		xml_node<>* root = doc.allocate_node(node_element, "entities");
		doc.append_node(root);

		for (int i = 0; i < entities_to_save.size(); ++i) {

			//Entity + id
			xml_node<>* child_node = doc.allocate_node(node_element, "entity");
			char * id_value = doc.allocate_string(std::to_string(entities_to_save[i]->id).c_str());
			child_node->append_attribute(doc.allocate_attribute("id", id_value));


			//Name
			xml_node<>* name_node = doc.allocate_node(node_element, "name");
			char * name_value = doc.allocate_string(entities_to_save[i]->name.c_str());
			name_node->value(name_value);
			child_node->append_node(name_node);

			//Position
			xml_node<>* pos_node = doc.allocate_node(node_element, "position");
			std::string pos_string = std::to_string(entities_to_save[i]->pos_x) + "," + std::to_string(entities_to_save[i]->pos_y) + "," + std::to_string(entities_to_save[i]->pos_z);
			char * pos_value = doc.allocate_string(pos_string.c_str());
			pos_node->value(pos_value);
			child_node->append_node(pos_node);

			//Append new entity
			root->append_node(child_node);

		}

		//std::string xml_as_string;
		//rapidxml::print(std::back_inserter(xml_as_string), doc);

		//Save to file
		std::ofstream file_stored(path.c_str());
		file_stored << doc;
		file_stored.close();
		doc.clear();

	}

}