//Author: Paula Jim�nez Benito
//Date: 2019

#pragma once


#include "Xml_Parser.h"
using namespace parser;


extern "C" __declspec(dllexport)
Xml_Parser* create_parser() {
	return new Xml_Parser();
}

extern "C" __declspec(dllexport)
void load(Xml_Parser* xml_parser, char* path) {
	xml_parser->load(path);
}

extern "C" __declspec(dllexport)
char* get_name(Xml_Parser* xml_parser, int id) {

	if (id < xml_parser->get_loaded_entities_count() && xml_parser->get_loaded_entities_count() != 0 && id >= 0) {
		
		std::string str = xml_parser->get_name(id);
		char *cstr = new char[str.length() + 1];
		//strcpy(cstr, str.c_str());
		strcpy_s(cstr, sizeof(std::string), str.c_str());
		return cstr;
	}
	else {
		return nullptr;
	}

}

extern "C" __declspec(dllexport)
float get_pos_x(Xml_Parser* xml_parser, int id) {

	if (id < xml_parser->get_loaded_entities_count() && xml_parser->get_loaded_entities_count() != 0 && id >= 0) {
		return xml_parser->get_pos_x(id);
	}
	else {
		return -1;
	}

}

extern "C" __declspec(dllexport)
float get_pos_y(Xml_Parser* xml_parser, int id) {

	if (id < xml_parser->get_loaded_entities_count() && xml_parser->get_loaded_entities_count() != 0 && id >= 0) {
		return xml_parser->get_pos_y(id);
	}
	else {
		return -1;
	}

}

extern "C" __declspec(dllexport)
float get_pos_z(Xml_Parser* xml_parser, int id) {

	if (id < xml_parser->get_loaded_entities_count() && xml_parser->get_loaded_entities_count() != 0 && id >= 0) {
		return xml_parser->get_pos_z(id);
	}
	else {
		return -1;
	}

}

extern "C" __declspec(dllexport)
int get_loaded_entities_count(Xml_Parser* xml_parser) {
	return xml_parser->get_loaded_entities_count();
}

extern "C" __declspec(dllexport)
void clear_saved(Xml_Parser* xml_parser) {
	xml_parser->clear_saved();
}

extern "C" __declspec(dllexport)
void add_entity_to_save(Xml_Parser* xml_parser, int id, char* name, float pos_x, float pos_y, float pos_z) {
	xml_parser->add_entity_to_save(id, name, pos_x, pos_y, pos_z);
}


extern "C" __declspec(dllexport)
void save(Xml_Parser* xml_parser, char* path) {
	xml_parser->save(path);
}

