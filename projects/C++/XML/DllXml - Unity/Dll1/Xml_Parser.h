//Author: Paula Jim�nez Benito
//Date: 2019

#pragma once

#include "stdafx.h"

#include <vector>
#include <string>

#include "rapidxml.hpp"
using namespace rapidxml;


namespace parser {

	class Xml_Parser {

		/**
		* @brief Entity struct data
		*
		*/
		struct Entity {
			/**
			* @brief Entity's id
			*
			*/
			int id;
			/**
			* @brief Entity's name
			*
			*/
			std::string name;
			/**
			* @brief Entity's position x
			*
			*/
			float pos_x;
			/**
			* @brief Entity's position y
			*
			*/
			float pos_y;
			/**
			* @brief Entity's position z
			*
			*/
			float pos_z;

			/**
			* @brief Construct a new Entity object
			*
			* @param id
			* @param name
			* @param pos_x
			* @param pos_y
			* @param pos_z
			*/
			Entity(int id, std::string name, float pos_x, float pos_y, float pos_z) :
				id(id), name(name), pos_x(pos_x), pos_y(pos_y), pos_z(pos_z) {}
		};


	private:

		/**
		* @brief Entities loaded from xml
		*
		*/
		std::vector<Entity*> entities_loaded;
		/**
		* @brief Entities to save into xml
		*
		*/
		std::vector<Entity*> entities_to_save;


	public:

		/**
		* @brief Construct a new Xml_Parser object
		*
		*/
		Xml_Parser() = default;

		/**
		* @brief Get the loaded entities count
		*
		* @return int
		*/
		int get_loaded_entities_count() {
			return (int)entities_loaded.size();
		}


	private:

		//Read
		/**
		* @brief Load xml
		*
		* @param path
		*/
		void load_xml(std::string path);
		/**
		* @brief Parse entity
		*
		* @param entity_tag
		*/
		void parse_entity(xml_node< >* entity_tag);

		//Write
		/**
		* @brief Write data into xml
		*
		* @param path
		*/
		void write_into_xml(std::string path);


	public:

		//Dll
		/**
		* @brief Load xml from path
		*
		* @param path
		*/
		void load(std::string path) {			//In no dll version, there is a check if the file exists. In this case, Unity realizes that check
			entities_loaded.clear();
			load_xml(path);
		}
		/**
		* @brief Get the name object
		*
		* @param id
		* @return std::string
		*/
		std::string get_name(int id) {
			return entities_loaded[id]->name;
		}
		/**
		* @brief Get the pos x object
		*
		* @param id
		* @return float
		*/
		float get_pos_x(int id) {
			return entities_loaded[id]->pos_x;
		}
		/**
		* @brief Get the pos y object
		*
		* @param id
		* @return float
		*/
		float get_pos_y(int id) {
			return entities_loaded[id]->pos_y;
		}
		/**
		* @brief Get the pos z object
		*
		* @param id
		* @return float
		*/
		float get_pos_z(int id) {
			return entities_loaded[id]->pos_z;
		}

		//Save
		/**
		* @brief Clear saved data
		*
		*/
		void clear_saved() {
			entities_to_save.clear();
		}
		/**
		* @brief Add entity to save into xml
		*
		* @param id
		* @param name
		* @param pos_x
		* @param pos_y
		* @param pos_z
		*/
		void add_entity_to_save(int id, std::string name, float pos_x, float pos_y, float pos_z) {
			entities_to_save.push_back(new Entity(id, name, pos_x, pos_y, pos_z));
		}
		/**
		* @brief Save data into xml in a path
		*
		* @param path
		*/
		void save(std::string path) {
			write_into_xml(path);
		}
		
	};

}