/********************************************************************************
** Form generated from reading UI file 'UnityInterface.ui'
**
** Created by: Qt User Interface Compiler version 5.12.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UNITYINTERFACE_H
#define UI_UNITYINTERFACE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UnityInterfaceClass
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QLabel *label_2;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_4;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_6;
    QTextEdit *textEdit_3;
    QTextEdit *textEdit_4;
    QPushButton *pushButton_9;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_7;
    QLabel *label_6;
    QLabel *label_7;
    QVBoxLayout *verticalLayout_8;
    QTextEdit *textEdit_5;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_10;
    QDoubleSpinBox *doubleSpinBox_2;
    QLabel *label_11;
    QDoubleSpinBox *doubleSpinBox_3;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QScrollBar *verticalScrollBar;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *UnityInterfaceClass)
    {
        if (UnityInterfaceClass->objectName().isEmpty())
            UnityInterfaceClass->setObjectName(QString::fromUtf8("UnityInterfaceClass"));
        UnityInterfaceClass->resize(455, 424);
        centralWidget = new QWidget(UnityInterfaceClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 20, 111, 16));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        verticalLayoutWidget_2 = new QWidget(centralWidget);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(20, 100, 381, 51));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setSpacing(1);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        pushButton_3 = new QPushButton(verticalLayoutWidget_2);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout_2->addWidget(pushButton_3);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(1);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        pushButton_2 = new QPushButton(verticalLayoutWidget_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        horizontalLayout_3->addWidget(pushButton_2);

        pushButton = new QPushButton(verticalLayoutWidget_2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_3->addWidget(pushButton);


        verticalLayout_2->addLayout(horizontalLayout_3);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 170, 51, 21));
        label_2->setFont(font);
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(20, 40, 381, 51));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(1);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(1);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_4 = new QLabel(horizontalLayoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_5->addWidget(label_4);

        label_5 = new QLabel(horizontalLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_5->addWidget(label_5);


        horizontalLayout->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(1);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        textEdit_3 = new QTextEdit(horizontalLayoutWidget);
        textEdit_3->setObjectName(QString::fromUtf8("textEdit_3"));

        verticalLayout_6->addWidget(textEdit_3);

        textEdit_4 = new QTextEdit(horizontalLayoutWidget);
        textEdit_4->setObjectName(QString::fromUtf8("textEdit_4"));

        verticalLayout_6->addWidget(textEdit_4);


        horizontalLayout->addLayout(verticalLayout_6);

        pushButton_9 = new QPushButton(centralWidget);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setGeometry(QRect(20, 300, 381, 23));
        verticalLayoutWidget = new QWidget(centralWidget);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 190, 381, 91));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(1);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(1);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        label_6 = new QLabel(verticalLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        verticalLayout_7->addWidget(label_6);

        label_7 = new QLabel(verticalLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout_7->addWidget(label_7);


        horizontalLayout_2->addLayout(verticalLayout_7);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setSpacing(1);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        textEdit_5 = new QTextEdit(verticalLayoutWidget);
        textEdit_5->setObjectName(QString::fromUtf8("textEdit_5"));

        verticalLayout_8->addWidget(textEdit_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(1);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_4->addWidget(label_3);

        doubleSpinBox = new QDoubleSpinBox(verticalLayoutWidget);
        doubleSpinBox->setObjectName(QString::fromUtf8("doubleSpinBox"));

        horizontalLayout_4->addWidget(doubleSpinBox);

        label_10 = new QLabel(verticalLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_4->addWidget(label_10);

        doubleSpinBox_2 = new QDoubleSpinBox(verticalLayoutWidget);
        doubleSpinBox_2->setObjectName(QString::fromUtf8("doubleSpinBox_2"));

        horizontalLayout_4->addWidget(doubleSpinBox_2);

        label_11 = new QLabel(verticalLayoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        horizontalLayout_4->addWidget(label_11);

        doubleSpinBox_3 = new QDoubleSpinBox(verticalLayoutWidget);
        doubleSpinBox_3->setObjectName(QString::fromUtf8("doubleSpinBox_3"));

        horizontalLayout_4->addWidget(doubleSpinBox_3);


        verticalLayout_8->addLayout(horizontalLayout_4);


        horizontalLayout_2->addLayout(verticalLayout_8);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(1);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        pushButton_7 = new QPushButton(verticalLayoutWidget);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));

        horizontalLayout_8->addWidget(pushButton_7);

        pushButton_8 = new QPushButton(verticalLayoutWidget);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));

        horizontalLayout_8->addWidget(pushButton_8);


        verticalLayout->addLayout(horizontalLayout_8);

        verticalScrollBar = new QScrollBar(centralWidget);
        verticalScrollBar->setObjectName(QString::fromUtf8("verticalScrollBar"));
        verticalScrollBar->setGeometry(QRect(420, 40, 16, 291));
        verticalScrollBar->setOrientation(Qt::Vertical);
        UnityInterfaceClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(UnityInterfaceClass);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 455, 21));
        UnityInterfaceClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(UnityInterfaceClass);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        UnityInterfaceClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(UnityInterfaceClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        UnityInterfaceClass->setStatusBar(statusBar);

        retranslateUi(UnityInterfaceClass);

        QMetaObject::connectSlotsByName(UnityInterfaceClass);
    } // setupUi

    void retranslateUi(QMainWindow *UnityInterfaceClass)
    {
        UnityInterfaceClass->setWindowTitle(QApplication::translate("UnityInterfaceClass", "UnityInterface", nullptr));
        label->setText(QApplication::translate("UnityInterfaceClass", "Database options", nullptr));
        pushButton_3->setText(QApplication::translate("UnityInterfaceClass", "Connect", nullptr));
        pushButton_2->setText(QApplication::translate("UnityInterfaceClass", "Save into DB", nullptr));
        pushButton->setText(QApplication::translate("UnityInterfaceClass", "Load from DB", nullptr));
        label_2->setText(QApplication::translate("UnityInterfaceClass", "Row 0", nullptr));
        label_4->setText(QApplication::translate("UnityInterfaceClass", "Database name                  ", nullptr));
        label_5->setText(QApplication::translate("UnityInterfaceClass", "Database table           ", nullptr));
        pushButton_9->setText(QApplication::translate("UnityInterfaceClass", "Add row", nullptr));
        label_6->setText(QApplication::translate("UnityInterfaceClass", "Name", nullptr));
        label_7->setText(QApplication::translate("UnityInterfaceClass", "Position                              ", nullptr));
        label_3->setText(QApplication::translate("UnityInterfaceClass", "    X", nullptr));
        label_10->setText(QApplication::translate("UnityInterfaceClass", "    Y", nullptr));
        label_11->setText(QApplication::translate("UnityInterfaceClass", "    Z", nullptr));
        pushButton_7->setText(QApplication::translate("UnityInterfaceClass", "Set to selected objet", nullptr));
        pushButton_8->setText(QApplication::translate("UnityInterfaceClass", "Delete row", nullptr));
    } // retranslateUi

};

namespace Ui {
    class UnityInterfaceClass: public Ui_UnityInterfaceClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UNITYINTERFACE_H
