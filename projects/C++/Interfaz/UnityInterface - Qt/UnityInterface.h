#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_UnityInterface.h"

class UnityInterface : public QMainWindow
{
	Q_OBJECT

public:
	UnityInterface(QWidget *parent = Q_NULLPTR);

private:
	Ui::UnityInterfaceClass ui;
};
