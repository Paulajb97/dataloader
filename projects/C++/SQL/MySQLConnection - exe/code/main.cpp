//Author: Paula Jim�nez Benito
//Date: 2019


#include "headers\Query\Query.hpp"
using namespace query;

#include <iostream>
using namespace std;


int main(int argc, char *argv[])
{

	//Create from command args
	Query query(argv[1], argv[2]);

	//Create from string
	//Query query("database_test", "player");

	//Test
	//query.clear();
	//query.insert_value("Name", "hi");
	//query.update_value(0, "Pos_X", 1.f);
	//query.update_value(0, "Pos_Y", 2.f);
	//query.update_value(0, "Pos_Z", 3.f);

	if (query.is_connected()) {

		cout << "Configurations saved: " << query.get_data_count() << endl;
		cout << "-----------------------------------------------\n" << endl;

		for (int i = 0; i < query.get_data_count(); ++i) {

			cout << "--- Entity " << i << " ---" << endl;
			cout << "Name: " << query.get_string(i, "Name") << endl;
			cout << "Position: " << query.get_float(i, "Pos_X") << ", " << query.get_float(i, "Pos_Y") << ", " << query.get_float(i, "Pos_Z") << endl;
			cout << endl;
		}

	}

	


	cin.get();
	return 0;

}
























////#include "my_global.h"
//#include "mysql.h" 
////#include <time.h>
////#include <stdio.h>
//#include <ctime>
//
//#include <iostream>
//using namespace std;
//
//#define SERVER "localhost"
//#define USER "root"
//#define PASSWORD ""
//#define DATABASE "database_test" 
//
//int main()
//{
//	
//	char ch;
//	//clock_t begin, end;
//	
//	MYSQL *connect=mysql_init(NULL); 
//	connect=mysql_real_connect(connect,SERVER,USER,PASSWORD,DATABASE,0,NULL,0);
//	if (!connect) {
//		cout << "MySQL Initialization or Connection Failed!" << endl; 
//		cin.get();
//		return 0; 
//	}
//	//mysql_query(connect,"insert into test values(2)");
//	
//	//mysql_query(connect,"INSERT INTO test (Number) VALUES(3)");
//
//	//begin = clock();
//
//	MYSQL_RES *res_set;
//	MYSQL_ROW row;
//	
//	mysql_query(connect, "SELECT * FROM test");
//	res_set = mysql_store_result(connect);
//	unsigned int numrows = mysql_num_rows(res_set);   
//	
//	while (row = mysql_fetch_row(res_set)) {
//
//		cout << "Res: " << row[0] << endl;	/* Print the row data */
//	}
//	cout << endl;
//	
//	//end = clock();
//	//cout << "Execution Time:" << (double)(end - begin) / CLOCKS_PER_SEC <<  "seconds" << endl;
//	mysql_close(connect);
//
//	cout << &ch << endl;
//
//	//getch();
//	//cin>>ch;
//
//
//	cin.get();
//	return 0;
//
//}





















////Standard C++ includes
//#include <stdlib.h>
//#include <iostream>
//
//
//
//
////MySQL includes
//#include "jdbc/mysql_connection.h"
//
//
//#include "jdbc/cppconn/driver.h"
//#include "jdbc/cppconn/exception.h"
//#include "jdbc/cppconn/resultset.h"
//#include "jdbc/cppconn/statement.h"
//
//using namespace std;
//
//int main(void)
//{
//	cout << endl;
//	cout << "Running 'SELECT 'Hello World!' "
//		"AS _message'..." << endl;
//	
//	try {
//		sql::Driver *driver;
//		sql::Connection *con;
//		sql::Statement *stmt;
//		sql::ResultSet *res;
//
//		/* Create a connection */
//		driver = get_driver_instance();
//		con = driver->connect("tcp://127.0.0.1:3306", "root", "root");
//
//		/* Connect to the MySQL test database */
//		con->setSchema("test");
//
//		stmt = con->createStatement();
//		res = stmt->executeQuery("SELECT 'Hello World!' AS _message");
//
//		while (res->next()) {
//
//			cout << "\t... MySQL replies: ";
//
//			/* Access column data by alias or column name */
//			cout << res->getString("_message") << endl;
//			cout << "\t... MySQL says it again: ";
//
//			/* Access column data by numeric offset, 1 is the first column */
//			cout << res->getString(1) << endl;
//
//		}
//		delete res;
//		delete stmt;
//		delete con;
//
//	}
//	catch (sql::SQLException &e) {
//		cout << "# ERR: SQLException in " << __FILE__;
//		cout << "(" << __FUNCTION__ << ") on line "
//			<< __LINE__ << endl;
//		cout << "# ERR: " << e.what();
//		cout << " (MySQL error code: " << e.getErrorCode();
//		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
//	}
//
//	cout << endl;
//
//	return EXIT_SUCCESS;
//
//}