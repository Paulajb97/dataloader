//Author: Paula Jim�nez Benito
//Date: 2019

#pragma once


#include "Query\Query.hpp"
using namespace query;


extern "C" __declspec(dllexport)
Query* create_query(std::string database, std::string table) {
	return new Query(database, table);
}


extern "C" __declspec(dllexport)
void set_new_row(Query* query, std::string name, float pos_x, float pos_y, float pos_z) {
	
	query->insert_value("Name", name);

	int index = query->get_int(name, "Id");
	query->update_value(index, "Pos_X", pos_x);
	query->update_value(index, "Pos_Y", pos_y);
	query->update_value(index, "Pos_Z", pos_z);

}


extern "C" __declspec(dllexport)
void update_row(Query* query, int index, std::string name, float pos_x, float pos_y, float pos_z) {	//No necesaria

	query->update_value(index, "Name", name);
	query->update_value(index, "Pos_X", pos_x);
	query->update_value(index, "Pos_Y", pos_y);
	query->update_value(index, "Pos_Z", pos_z);

}


extern "C" __declspec(dllexport)
void clear_table(Query* query) {

	query->clear();

}


extern "C" __declspec(dllexport)
std::string get_name(Query* query, int index) {

	return query->get_string(index, "Name");

}

extern "C" __declspec(dllexport)
float get_pos_x(Query* query, int index) {

	return query->get_float(index, "Pos_X");

}


extern "C" __declspec(dllexport)
float get_pos_y(Query* query, int index) {

	return query->get_float(index, "Pos_Y");

}


extern "C" __declspec(dllexport)
float get_pos_z(Query* query, int index) {

	return query->get_float(index, "Pos_Z");

}


extern "C" __declspec(dllexport)
int get_data_count(Query* query) {
	return query->get_data_count();
}
