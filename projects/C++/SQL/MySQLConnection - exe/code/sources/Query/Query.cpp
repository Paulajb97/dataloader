//Author: Paula Jim�nez Benito
//Date: 2019


#include "..\..\headers\Query\Query.hpp"

#include "mysql.h"

#include <iostream>
using namespace std;


namespace query {

	Query::Query(std::string database_name, std::string table_name, bool create_if_failed) :
		database_name(database_name),
		table_name(table_name),
		data_count(0)
	{

		set_param_map();

		open_connection(create_if_failed);

		if (is_connected()) {
			//Table check if exists
			if (get_value(0, "Name") == nullptr) {
				create_default_table();
			}
		}
		
	}

	void Query::open_connection(bool create_if_failed)
	{
		connect_to_database(database_name, create_if_failed);
	}

	void Query::close_connection()
	{
		mysql_close(connect);
	}


	void Query::insert_value(std::string name_param, int value)
	{
		insert_into_table(table_name, name_param, value);
	}

	void Query::insert_value(std::string name_param, std::string value)
	{
		insert_into_table(table_name, name_param, value);
	}

	void Query::insert_value(std::string name_param, float value)
	{
		insert_into_table(table_name, name_param, value);
	}

	void Query::update_value(int id, std::string name_param, int value)
	{
		update_value_from_table(table_name, id, name_param, value);
	}

	void Query::update_value(int id, std::string name_param, std::string value)
	{
		update_value_from_table(table_name, id, name_param, value);
	}

	void Query::update_value(int id, std::string name_param, float value)
	{
		update_value_from_table(table_name, id, name_param, value);
	}


	int Query::get_int(int id, std::string name_param)
	{
		return std::stoi(get_value(id, name_param)[0]);
	}

	float Query::get_float(int id, std::string name_param)
	{
		return std::stof(get_value(id, name_param)[0]);
	}

	std::string Query::get_string(int id, std::string name_param)
	{
		return get_value(id, name_param)[0];
	}

	int Query::get_int(std::string name, std::string name_param)
	{
		return std::stoi(get_value(name_param, "Name", name)[0]);
	}

	float Query::get_float(std::string name, std::string name_param)
	{
		return std::stof(get_value(name_param, "Name", name)[0]);
	}

	std::string Query::get_string(std::string name, std::string name_param)
	{
		return get_value(name_param, "Name", name)[0];
	}

	void Query::clear()
	{
		clear_table(table_name);
	}

	int Query::get_data_count()
	{

		MYSQL_RES *res_set;
		MYSQL_ROW row;

		send_query("SELECT COUNT(Id) FROM " + table_name);
		res_set = mysql_store_result(connect);

		if (res_set != nullptr) {
			row = mysql_fetch_row(res_set);
			return std::stoi(row[0]);
		}
		else {
			return -1;
		}

		//return data_count;

	}


	void Query::create_database(std::string name)
	{
		send_query("CREATE DATABASE" + name);
	}

	void Query::delete_database(std::string name)
	{
		send_query("DROP DATABASE" + name);
	}

	void Query::create_table(std::string name, std::vector<std::pair<std::string, Param_type>> columns)
	{


		std::string query_init = "CREATE TABLE " + name + "(";

		std::string query_centre = "";
		
		for (int i = 0; i < columns.size(); ++i) {

			query_centre += columns[i].first + " ";
			query_centre += type_data_string[columns[i].second];
			
			query_centre += i==columns.size()-1 ? ")" : ", ";
		}

		send_query(query_init + query_centre);

	}

	void Query::delete_table(std::string name)
	{
		send_query("DROP TABLE " + name);
	}

	void Query::clear_table(std::string name)
	{
		send_query("DELETE FROM " + name);
	}

	void Query::connect_to_database(std::string name, bool create_if_failed)
	{

		connect = mysql_init(NULL);
		connect = mysql_real_connect(connect, SERVER, USER, PASSWORD, name.c_str(), 0, NULL, 0);
		if (!connect) {
			cout << "MySQL Initialization or Connection Failed!" << endl;

			//if (create_if_failed) {

			//	connect = mysql_init(NULL);
			//	connect = mysql_real_connect(connect, SERVER, USER, PASSWORD, "mysql", 0, NULL, 0);

			//	create_default_database();
			//	create_default_table();
			//	connect_to_database(database_name);

			//}

		}

	}

	void Query::disconnect_from_database(std::string name)
	{
		mysql_close(connect);
	}


	MYSQL_ROW Query::get_value(int id, std::string name_param)
	{
		return get_value(name_param, "Id", to_string(id));
	}

	MYSQL_ROW Query::get_value(std::string param_to_get, std::string name_column, std::string name_to_search)
	{
		
		MYSQL_RES *res_set;
		MYSQL_ROW row;

		send_query("SELECT " + param_to_get + " FROM " + table_name + " WHERE " + name_column + "='" + name_to_search + "'");
		res_set = mysql_store_result(connect);

		if (res_set != nullptr) {
			row = mysql_fetch_row(res_set);
			return row;
		}
		else {
			return nullptr;
		}

	}


	void Query::insert_into_table(std::string name_table, std::string name_param, int value)
	{
		send_query("INSERT INTO " + name_table + " (Id, " + name_param + ") VALUES(" + to_string(data_count) + ", " + to_string(value) + ")");
		++data_count;
	}

	void Query::insert_into_table(std::string name_table, std::string name_param, std::string value)
	{
		send_query("INSERT INTO " + name_table + " (Id, " + name_param + ") VALUES(" + to_string(data_count) + ", '" + value + "')");
		++data_count;
	}

	void Query::insert_into_table(std::string name_table, std::string name_param, float value)
	{
		send_query("INSERT INTO " + name_table + " (Id, " + name_param + ") VALUES(" + to_string(data_count) + ", " + to_string(value) + ")");
		++data_count;
	}


	void Query::update_value_from_table(std::string name_table, int id, std::string name_param, int value)
	{
		send_query("UPDATE " + name_table + " SET " + name_param + "=" + to_string(value) + " WHERE ID=" + to_string(id));
	}

	void Query::update_value_from_table(std::string name_table, int id, std::string name_param, std::string value)
	{
		send_query("UPDATE " + name_table + " SET " + name_param + "='" + value + "' WHERE ID=" + to_string(id));
	}

	void Query::update_value_from_table(std::string name_table, int id, std::string name_param, float value)
	{
		send_query("UPDATE " + name_table + " SET " + name_param + "=" + to_string(value) + " WHERE ID=" + to_string(id));
	}


	void Query::delete_row_from_table(std::string name_table, int id)
	{
		send_query("DELETE FROM " + name_table + " WHERE ID=" + to_string(id));
	}

	bool Query::send_query(std::string query)
	{

		try {
			mysql_query(connect, query.c_str());
		}
		catch (...) {
			cout << "Query FAILED: " << query.c_str() << endl;
			return false;
		}
		
		return true;

	}

	void Query::set_param_map()
	{

		type_data_string[INT] = "int";
		type_data_string[FLOAT] = "float";
		type_data_string[TEXT] = "text";

	}

	void Query::create_default_database()
	{
		create_database(database_name);
	}

	void Query::create_default_table()
	{

		vector<pair<std::string, Query::Param_type>> columns_for_database;

		columns_for_database.push_back(make_pair("Id", Query::Param_type::INT));
		columns_for_database.push_back(make_pair("Name", Query::Param_type::TEXT));
		columns_for_database.push_back(make_pair("Pos_X", Query::Param_type::FLOAT));
		columns_for_database.push_back(make_pair("Pos_Y", Query::Param_type::FLOAT));
		columns_for_database.push_back(make_pair("Pos_Z", Query::Param_type::FLOAT));


		columns["Id"] = Param_type::INT;
		columns["Name"] = Param_type::TEXT;
		columns["Pos_X"] = Param_type::FLOAT;
		columns["Pos_Y"] = Param_type::FLOAT;
		columns["Pos_Z"] = Param_type::FLOAT;


		create_table(table_name, columns_for_database);

	}

}