//Author: Paula Jim�nez Benito
//Date: 2019

#pragma once


#include "stdafx.h"

#include <string>
#include <map>
#include <vector>

#define SERVER "localhost"
#define USER "root"
#define PASSWORD ""


struct MYSQL;
typedef char **MYSQL_ROW;


namespace query {

	class Query {

	public:

		/**
		* @brief Database's parameters' type
		*
		*/
		enum Param_type {
			INT,
			FLOAT,
			TEXT
		};


	private:

		/**
		* @brief Database type asstring
		*
		*/
		std::map<Param_type, std::string> type_data_string;

		/**
		* @brief Database's name
		*
		*/
		std::string database_name;
		/**
		* @brief Table's name
		*
		*/
		std::string table_name;
		/**
		* @brief Columns' data
		*
		*/
		std::map<std::string, Param_type> columns;

		/**
		* @brief Number of rows saved
		*
		*/
		int data_count;

		/**
		* @brief Connection to the database
		*
		*/
		MYSQL* connect;


	public:

		/**
		* @brief Construct a new Query object
		*
		* @param database_name
		* @param table_name
		* @param create_if_failed
		*/
		Query(std::string database_name, std::string table_name, bool create_if_failed = false);
		/**
		* @brief Destroy the Query object
		*
		*/
		~Query() {
			close_connection();
		}

		/**
		* @brief Open connection with database
		*
		* @param create_if_failed
		*/
		void open_connection(bool create_if_failed = false);
		/**
		* @brief Close connection with database
		*
		*/
		void close_connection();
		/**
		* @brief Check if there is a connection
		*
		* @return true
		* @return false
		*/
		bool is_connected() {
			return connect;
		}

		/**
		* @brief Insert int value
		*
		* @param name_param
		* @param value
		*/
		void insert_value(std::string name_param, int value);
		/**
		* @brief Insert string value
		*
		* @param name_param
		* @param value
		*/
		void insert_value(std::string name_param, std::string value);
		/**
		* @brief Insert float value
		*
		* @param name_param
		* @param value
		*/
		void insert_value(std::string name_param, float value);

		/**
		* @brief Update int value
		*
		* @param id
		* @param name_param
		* @param value
		*/
		void update_value(int id, std::string name_param, int value);
		/**
		* @brief Update string value
		*
		* @param id
		* @param name_param
		* @param value
		*/
		void update_value(int id, std::string name_param, std::string value);
		/**
		* @brief Update float value
		*
		* @param id
		* @param name_param
		* @param value
		*/
		void update_value(int id, std::string name_param, float value);

		/**
		* @brief Get the int value by int id
		*
		* @param id
		* @param name_param
		* @return int
		*/
		int			get_int(int id, std::string name_param);
		/**
		* @brief Get the float value by int id
		*
		* @param id
		* @param name_param
		* @return float
		*/
		float		get_float(int id, std::string name_param);
		/**
		* @brief Get the string value by int id
		*
		* @param id
		* @param name_param
		* @return std::string
		*/
		std::string get_string(int id, std::string name_param);

		/**
		* @brief Get the int value by string id
		*
		* @param name
		* @param name_param
		* @return int
		*/
		int			get_int(std::string name, std::string name_param);
		/**
		* @brief Get the float value by int id
		*
		* @param name
		* @param name_param
		* @return float
		*/
		float		get_float(std::string name, std::string name_param);
		/**
		* @brief Get the string value by int id
		*
		* @param name
		* @param name_param
		* @return std::string
		*/
		std::string get_string(std::string name, std::string name_param);

		/**
		* @brief Clear all table's data
		*
		*/
		void clear();

		/**
		* @brief Get the data count object
		*
		* @return int
		*/
		int get_data_count();


	private:

		/**
		* @brief Create a database object
		*
		* @param name
		*/
		void create_database(std::string name);
		/**
		* @brief Delete database
		*
		* @param name
		*/
		void delete_database(std::string name);

		/**
		* @brief Create a table object
		*
		* @param name
		* @param columns
		*/
		void create_table(std::string name, std::vector<std::pair<std::string, Param_type>> columns);
		/**
		* @brief Delete table
		*
		* @param name
		*/
		void delete_table(std::string name);
		/**
		* @brief Clear table's data
		*
		* @param name
		*/
		void clear_table(std::string name);

		/**
		* @brief Connect to a database
		*
		* @param name
		* @param create_if_failed
		*/
		void connect_to_database(std::string name, bool create_if_failed = false);
		/**
		* @brief Disconnect from a database
		*
		* @param name
		*/
		void disconnect_from_database(std::string name);	//REVISAR


															/**
															* @brief Get the value with int id
															*
															* @param id
															* @param name_param
															* @return MYSQL_ROW
															*/
		MYSQL_ROW get_value(int id, std::string name_param);
		/**
		* @brief Get the value by general method
		*
		* @param param_to_get
		* @param name_column
		* @param name_to_search
		* @return MYSQL_ROW
		*/
		MYSQL_ROW get_value(std::string param_to_get, std::string name_column, std::string name_to_search);

		/**
		* @brief Insert int value into table
		*
		* @param name_table
		* @param name_param
		* @param value
		*/
		void insert_into_table(std::string name_table, std::string name_param, int value);
		/**
		* @brief Insert string value into table
		*
		* @param name_table
		* @param name_param
		* @param value
		*/
		void insert_into_table(std::string name_table, std::string name_param, std::string value);
		/**
		* @brief Insert float value into table
		*
		* @param name_table
		* @param name_param
		* @param value
		*/
		void insert_into_table(std::string name_table, std::string name_param, float value);

		/**
		* @brief Update int value from table
		*
		* @param name_table
		* @param id
		* @param name_param
		* @param value
		*/
		void update_value_from_table(std::string name_table, int id, std::string name_param, int value);
		/**
		* @brief Update string value from table
		*
		* @param name_table
		* @param id
		* @param name_param
		* @param value
		*/
		void update_value_from_table(std::string name_table, int id, std::string name_param, std::string value);
		/**
		* @brief Update float value from table
		*
		* @param name_table
		* @param id
		* @param name_param
		* @param value
		*/
		void update_value_from_table(std::string name_table, int id, std::string name_param, float value);

		/**
		* @brief Delete row
		*
		* @param name_table
		* @param id
		*/
		void delete_row_from_table(std::string name_table, int id);

		/**
		* @brief Send query
		*
		* @param query
		* @return true
		* @return false
		*/
		bool send_query(std::string query);


	private:

		/**
		* @brief Set the param map object
		*
		*/
		void set_param_map();
		/**
		* @brief Create a default database object
		*
		*/
		void create_default_database();


	public:

		/**
		* @brief Create a default table object
		*
		*/
		void create_default_table();

	};

}