//Author: Paula Jim�nez Benito
//Date: 2019

#pragma once


#include "Query.h"
using namespace query;

extern "C" __declspec(dllexport)
Query* create_query(char* database, char* table) {
	return new Query(database, table);
}


extern "C" __declspec(dllexport)
bool is_connected(Query* query) {
	return query->is_connected();
}


extern "C" __declspec(dllexport)
void set_new_row(Query* query, char* name, float pos_x, float pos_y, float pos_z) {

	query->insert_value("Name", name);

	int index = query->get_int(name, "Id");
	query->update_value(index, "Pos_X", pos_x);
	query->update_value(index, "Pos_Y", pos_y);
	query->update_value(index, "Pos_Z", pos_z);

}


extern "C" __declspec(dllexport)
void update_row(Query* query, int index, char* name, float pos_x, float pos_y, float pos_z) {	//No necesaria

	query->update_value(index, "Name", name);
	query->update_value(index, "Pos_X", pos_x);
	query->update_value(index, "Pos_Y", pos_y);
	query->update_value(index, "Pos_Z", pos_z);

}


extern "C" __declspec(dllexport)
void clear_table(Query* query) {

	query->clear();

}


extern "C" __declspec(dllexport)
char* get_name(Query* query, int index) {

	if (index < query->get_data_count() && query->get_data_count() != 0 && index >= 0) {

		std::string str = query->get_string(index, "Name");
		char *cstr = new char[str.length() + 1];
		//strcpy(cstr, str.c_str());
		strcpy_s(cstr, sizeof(std::string), str.c_str());
		return cstr;
	}
	else {
		return nullptr;
	}

}

extern "C" __declspec(dllexport)
float get_pos_x(Query* query, int index) {

	return query->get_float(index, "Pos_X");

}


extern "C" __declspec(dllexport)
float get_pos_y(Query* query, int index) {

	return query->get_float(index, "Pos_Y");

}


extern "C" __declspec(dllexport)
float get_pos_z(Query* query, int index) {

	return query->get_float(index, "Pos_Z");

}


extern "C" __declspec(dllexport)
int get_data_count(Query* query) {
	return query->get_data_count();
}
