﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

using CPEngine2.Native;

public unsafe class SQLConnection
{
    #region Native

    struct NativeSQLConnection { }

    const string dllName = "Dll1";

    //Create query
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern NativeSQLConnection* create_query(
        [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string database,
        [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string table);

    //Ceck if database is connected
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern bool is_connected(NativeSQLConnection* query);

    //Set new row
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void set_new_row(NativeSQLConnection* query,
         [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string name,
         float pos_x, float pos_y, float pos_z);

    //Update row    (OPTIONAL)
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void update_row(NativeSQLConnection* query,
        int index,
         [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string name,
        float pos_x, float pos_y, float pos_z);

    //Clear table
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void clear_table(NativeSQLConnection* query);

    //Get name
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr get_name(NativeSQLConnection* query, int index);

    //Get x position
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern float get_pos_x(NativeSQLConnection* query, int index);

    //Get y position
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern float get_pos_y(NativeSQLConnection* query, int index);

    //Get z position
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern float get_pos_z(NativeSQLConnection* query, int index);

    //Get data count
    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern int get_data_count(NativeSQLConnection* query);

    #endregion


    NativeSQLConnection* nativePointer;
    

    #region API friendly
    

    public SQLConnection() { }
    public SQLConnection(string database_name, string table_name)
    {
        nativePointer = create_query(database_name, table_name);
    }


    public bool IsConnected()
    {
        return is_connected(nativePointer);
    }

    
    public void SetNewRow(string name, float pos_x, float pos_y, float pos_z)
    {
        set_new_row(nativePointer, name, pos_x, pos_y, pos_z);
    }

    public void ClearTable()
    {
        clear_table(nativePointer);
    }

    public string GetName(int index)
    {

        IntPtr new_string_native = get_name(nativePointer, index);

        LPUtf8StrMarshaler marshal = new LPUtf8StrMarshaler(true);
        string new_string = (string)marshal.MarshalNativeToManaged(new_string_native);

        return new_string;

    }

    public float GetPosX(int index)
    {
        return get_pos_x(nativePointer, index);
    }

    public float GetPosY(int index)
    {
        return get_pos_y(nativePointer, index);
    }

    public float GetPosZ(int index)
    {
        return get_pos_z(nativePointer, index);
    }

    public int GetDataCount()
    {
        return get_data_count(nativePointer);
    }

    #endregion
}
