﻿using UnityEngine;

public class DatabaseRowTester : MonoBehaviour
{
    [SerializeField]
    DatabaseRow data;
    //public DatabaseRow[] data_array;


    private void Start()
    {
        data = new DatabaseRow();
    }


    public void SetData(string name, Vector3 position)
    {
        data.name = name;
        data.position = position;
    }



}
