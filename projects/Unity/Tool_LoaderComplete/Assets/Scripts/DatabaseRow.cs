﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class DatabaseRow
{
    public string name;
    public Vector3 position;

    public DatabaseRow(string name = "", Vector3 position = new Vector3())
    {
        this.name = name;
        this.position = position;
    }

}


[CustomPropertyDrawer(typeof(DatabaseRow))]
public class DatabaseRowDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label = EditorGUI.BeginProperty(position, label, property);

        Rect contentPosition = EditorGUI.PrefixLabel(position, label);

        if (position.height > 16f)
        {
            position.height = 16f;
            EditorGUI.indentLevel += 1;
            contentPosition = EditorGUI.IndentedRect(position);
            contentPosition.y += 18f;
        }

        contentPosition.width *= 0.25f;

        EditorGUI.indentLevel = 0;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("name"), GUIContent.none); //new GUIContent("Name"));
        contentPosition.x += contentPosition.width;
        contentPosition.width *= 3f;

        EditorGUIUtility.labelWidth = 14f;
        EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("position"), GUIContent.none);

        EditorGUI.EndProperty();
    }

}

