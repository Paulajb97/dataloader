﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

using CPEngine2.Native;

public unsafe class XmlConnection
{

    #region Native

    struct NativeXmlConnection { }

    const string dllName = "XmlParserDll";

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern NativeXmlConnection* create_parser();

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void load(NativeXmlConnection* parser,
        [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string path);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern IntPtr get_name(NativeXmlConnection* parser, int id);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern float get_pos_x(NativeXmlConnection* parser, int id);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern float get_pos_y(NativeXmlConnection* parser, int id);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern float get_pos_z(NativeXmlConnection* parser, int id);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern int get_loaded_entities_count(NativeXmlConnection* parser);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void clear_saved(NativeXmlConnection* parser);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void add_entity_to_save(NativeXmlConnection* parser, int id,
        [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string name,
        float pos_x, float pos_y, float pos_z);

    [DllImport(dllName, CallingConvention = CallingConvention.Cdecl)]
    private static extern void save(NativeXmlConnection* parser,
        [In()] [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(LPUtf8StrMarshaler))] string path);

    #endregion


    NativeXmlConnection* nativePointer;


    #region API friendly

    public XmlConnection()
    {
        nativePointer = create_parser();
    }

    //Read
    public void Load(string path)
    {
        load(nativePointer, path);
    }

    public string GetConfigName(int id)
    {

        IntPtr new_string_native = get_name(nativePointer, id);

        LPUtf8StrMarshaler marshal = new LPUtf8StrMarshaler(true);
        string new_string = (string)marshal.MarshalNativeToManaged(new_string_native);

        return new_string;

    }

    public float GetConfigPosX(int id)
    {
        return get_pos_x(nativePointer, id);
    }

    public float GetConfigPosY(int id)
    {
        return get_pos_y(nativePointer, id);
    }

    public float GetConfigPosZ(int id)
    {
        return get_pos_z(nativePointer, id);
    }

    public int GetLoadedEntitiesCount()
    {
        return get_loaded_entities_count(nativePointer);
    }

    //Write
    public void ClearEntitiesSaved()
    {
        clear_saved(nativePointer);
    }

    public void AddEntityToSave(int id, string name, float pos_x, float pos_y, float pos_z)
    {
        add_entity_to_save(nativePointer, id, name, pos_x, pos_y, pos_z);
    }

    public void Save(string path)
    {
        save(nativePointer, path);
    }

    #endregion

}
