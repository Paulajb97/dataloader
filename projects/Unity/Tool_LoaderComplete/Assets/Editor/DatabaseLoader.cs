﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class DatabaseLoader : EditorWindow
{

    List<DatabaseRow> data = new List<DatabaseRow>();
    Vector2 scrollPos;

    string databaseName = "database_test";
    string tableName = "player";

    SQLConnection connection = null;



    [MenuItem("Window/Data Loader/Database")]
    public static void ShowWindow()
    {
        GetWindow<DatabaseLoader>("DatabaseLoader");
    }

    //Window code
    void OnGUI()
    {

        //Scroll
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, true);


        EditorGUILayout.Space();


        //BBDD options
        GUILayout.Label("Database options", EditorStyles.boldLabel);

        databaseName = EditorGUILayout.TextField("Database name", databaseName);
        tableName = EditorGUILayout.TextField("Table name", tableName);
        if (GUILayout.Button("Connect"))                              //Connect to database button
        {
            CreateConnection();
        }

        //Horizontal buttons
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Load from DB"))                         //Load from bbdd button
        {
            LoadFromDB();
        }
        if (GUILayout.Button("Save into DB"))                         //Save into bbdd button
        {
            SaveIntoDB();
        }

        GUILayout.EndHorizontal();


        EditorGUILayout.Space();


        //Row data
        for (int i = 0; i < data.Count; i++)
        {

            GUILayout.Label("Row " + i, EditorStyles.boldLabel);

            //Data
            data[i].name = EditorGUILayout.TextField("Name", data[i].name);
            data[i].position = EditorGUILayout.Vector3Field("Position", data[i].position);

            //Horizontal buttons
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Set to selected object"))             //Set params to object button
            {
                SetParamsToObject(data[i].name, data[i].position);
            }
            if (GUILayout.Button("Delete row"))                         //Delete row button
            {
                data.RemoveAt(i);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
        }


        EditorGUILayout.Space();


        if (GUILayout.Button("Add row"))                                //Add row button
        {
            AddRow();
        }


        EditorGUILayout.Space();


        EditorGUILayout.EndScrollView();

    }


    void SetParamsToObject(string name, Vector3 position)
    {

        foreach (GameObject obj in Selection.gameObjects)
        {
            DatabaseRowTester tester = obj.GetComponent<DatabaseRowTester>();
            if (tester != null)
            {
                tester.SetData(name, position);
            }

        }

    }


    void AddRow()
    {
        DatabaseRow dataAux = new DatabaseRow();

        dataAux.name = "";
        dataAux.position = new Vector3();

        data.Add(dataAux);
    }


    //Database options

    void CreateConnection()
    {
        connection = new SQLConnection(databaseName, tableName);

        if (connection!=null && connection.IsConnected())
        {
            Debug.Log("Connected!");
        }
        else
        {
            Debug.Log("Connection failed");
        }

    }

    void LoadFromDB()
    {

        if (connection != null && connection.IsConnected())
        {
            data.Clear();

            int dataCount = connection.GetDataCount();

            for (int i = 0; i < dataCount; i++)
            {
                DatabaseRow dataAux = new DatabaseRow();

                dataAux.name = connection.GetName(i) != null ? connection.GetName(i) : "";
                dataAux.position = new Vector3(connection.GetPosX(i), connection.GetPosY(i), connection.GetPosZ(i));

                data.Add(dataAux);
            }

        }
        else
        {
            Debug.Log("There is no connection");
        }
        
    }

    void SaveIntoDB()
    {

        if (connection.IsConnected())
        {
            connection.ClearTable();

            for (int i = 0; i < data.Count; i++)
            {
                connection.SetNewRow(data[i].name, data[i].position.x, data[i].position.y, data[i].position.z);
            }
        }
        else
        {
            Debug.Log("There is no connection");
        }

    }
    
}
