﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

using System.IO;

public class XmlLoader : EditorWindow
{
    List<DatabaseRow> data = new List<DatabaseRow>();
    Vector2 scrollPos;

    string pathFolder = "Assets/Configs/";
    string fileName = "configs";
    string fileExtension = ".xml";


    [MenuItem("Window/Data Loader/Xml")]
    public static void ShowWindow()
    {
        GetWindow<XmlLoader>("XmlLoader");
    }

    //Window code
    void OnGUI()
    {

        //Scroll
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, true);


        EditorGUILayout.Space();


        //Xml options
        GUILayout.Label("Options", EditorStyles.boldLabel);

        fileName = EditorGUILayout.TextField("File name", fileName);


        //Horizontal buttons
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Load"))                         //Load from xml button
        {
            if (CheckIfFileExists())
            {
                LoadConfigs();
            }
            else
            {
                Debug.Log("ERROR: File not found");
            }
        }
        if (GUILayout.Button("Save"))                         //Save into xml button
        {
            SaveConfigs();
        }

        GUILayout.EndHorizontal();


        EditorGUILayout.Space();


        //Row data
        for (int i = 0; i < data.Count; i++)
        {

            GUILayout.Label("Configuration " + i, EditorStyles.boldLabel);

            //Data
            data[i].name = EditorGUILayout.TextField("Name", data[i].name);
            data[i].position = EditorGUILayout.Vector3Field("Position", data[i].position);

            //Horizontal buttons
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Set to selected object"))             //Set params to object button
            {
                SetParamsToObject(data[i].name, data[i].position);
            }
            if (GUILayout.Button("Delete row"))                         //Delete row button
            {
                data.RemoveAt(i);
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
        }


        EditorGUILayout.Space();


        if (GUILayout.Button("Add row"))                                //Add row button
        {
            AddRow();
        }


        EditorGUILayout.Space();


        EditorGUILayout.EndScrollView();

    }


    void SetParamsToObject(string name, Vector3 position)
    {

        //Get selected objects
        foreach (GameObject obj in Selection.gameObjects)
        {
            //Check if they have the correct script
            DatabaseRowTester tester = obj.GetComponent<DatabaseRowTester>();
            if (tester != null)
            {
                tester.SetData(name, position);
            }

        }

    }


    void AddRow(string name = "", Vector3 position = new Vector3())
    {
        DatabaseRow dataAux = new DatabaseRow();

        dataAux.name = name;
        dataAux.position = position;

        data.Add(dataAux);
    }


    //Xml options
    bool CheckIfFileExists()
    {
        return File.Exists(pathFolder + fileName + fileExtension);
    }

    void LoadConfigs()
    {

        //Reset data
        data.Clear();

        XmlConnection connection = new XmlConnection();

        //Load xml data
        string path = pathFolder + fileName + fileExtension;
        connection.Load(path);

        //Add new rows
        for (int i = 0; i < connection.GetLoadedEntitiesCount(); i++)
        {
            AddRow(connection.GetConfigName(i), new Vector3(connection.GetConfigPosX(i), connection.GetConfigPosY(i), connection.GetConfigPosZ(i)));
        }

    }

    void SaveConfigs()
    {

        XmlConnection connection = new XmlConnection();

        //Reset data
        connection.ClearEntitiesSaved();

        //Add entities/rows to save
        for (int i = 0; i < data.Count; i++)
        {
            connection.AddEntityToSave(i, data[i].name, data[i].position.x, data[i].position.y, data[i].position.z);
        }

        //Save data into xml
        string path = pathFolder + fileName + fileExtension;
        connection.Save(path);

    }
}
